package govern

type Playbook []Play

type Play struct {
	Name  string   `yaml:"name"`
	Hosts []string `yaml:"hosts"`
	Roles []string `yaml:"roles"`
}

type Role []Task

type Task struct {
	Name string            `yaml:"name"`
	Run  string            `yaml:"run"`
	Args map[string]string `yaml:"args"`
}
