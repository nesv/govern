package modules

type Request struct {
	ID   string
	Data []byte
}

type Reply struct {
	Status ReplyStatus
	Data   []byte
}

type ReplyStatus int

func (rs ReplyStatus) String() string {
	switch rs {
	case Success:
		return "Success"
	case Failed:
		return "Failed"
	}

	return ""
}

const (
	Success ReplyStatus = iota
	Failed
)
