package modules

type Test struct{}

func (m *Test) Ping(args *Request, reply *Reply) error {
	reply.Status = Success
	reply.Data = []byte("ok")
	return nil
}

func (m *Test) Echo(args *Request, reply *Reply) error {
	reply.Status = Success
	reply.Data = []byte(args.Data)
	return nil
}
