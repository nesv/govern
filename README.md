# govern

Yet another configuration management system.

This is more of a proof-of-concept project, than anything else. Who knows, maybe
it will become widely-used.

The concept I am trying to prove here, is to merge the speed/usefulness of
Saltstack, and the operational flexibility of Ansible. To illustrate this, in
Saltstack, it is a pain in the ass to share information (grains) between
minions; this requires setting up Salt's _mine_ facility. Having worked with
Ansible, all information about the managed hosts is gathered prior to tasks
being executed on the hosts, which make it supremely easy to pass the IP address
of one managed host, to another. The downside of Ansible's approach, is that
it is significantly slower than Salt.

So, what I hope to achieve with Govern, is a configuration management system
that feels like Ansible, but has the consistency and flexibility of Salt's SLS
files. Do not be surprised if you notice a lot of similarities between Govern,
Salt, and Ansible.

## How it works (design goals)

- The core `govern` executable will be a single file that includes all of the
  necessary "core" commands to run a fully-fledged Govern cluster
- Govern will have remote execution agents
- There is only one place to define something (playbook, task, variable, etc.)
- Govern plugins are seperate executables
- Playbooks are "compiled" on the local machine, not on the agents. The compiled
  playbooks are then sent to the agents via RPC.
- Use one declarative format for everything (YAML, most-likely).
- Everything can be templated (using Go's `text/template` package).

## TODO

`govern gather-facts` - Gathers facts about the system it is running on,
as well as loads any user-defined facts about the system (e.g. from a file).

`govern agent` - The process that runs on each of the managed machines, and
provides an RPC endpoint.

`govern ping` - Which remote hosts are currently online?

`govern run [-n|--pretend] <playbook>` - Compiles a playbook for each managed
host, and sends each agent a complete, ordered list of tasks to perform, with
any variable interpolation already done.

`govern ssh <host>` - Log into a managed host.

Govern agent RPC endpoints - this is what is going to be needed for the local
machine to be able to communicate with the remote Govern agents.
