package govern

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

func LoadInventory(pth string) (*Inventory, error) {
	v, err := ioutil.ReadFile(pth)
	if err != nil {
		return nil, err
	}

	var inv Inventory
	if err := yaml.Unmarshal(v, &inv); err != nil {
		return nil, err
	}

	return &inv, nil
}

type Inventory struct {
	Hosts  map[string]string    `yaml:"hosts"`
	Groups map[string]HostGroup `yaml:"groups"`
}

func (i Inventory) HostAddr(name string) string {
	v, ok := i.Hosts[name]
	if !ok {
		return ""
	}
	return v
}

type Host struct {
	Addr string `yaml:"host"`
}

type HostGroup struct {
	Hosts       []string      `yaml:"hosts"`
	ChildGroups []string      `yaml:"children"`
	Vars        HostGroupVars `yaml:"vars"`
}

type HostGroupVars map[string]interface{}
