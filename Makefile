all: govern

govern: $(shell find . -type f -iname "*.go")
	GOVENDOREXPERIMENT=1 go build -o $@ gitlab.com/nesv/govern/cmd/govern

clean:
	rm -f govern
