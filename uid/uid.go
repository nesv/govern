package uid

import (
	"math/rand"
	"time"
)

const (
	DefaultIDLength = 8
)

var (
	charset = []rune("abcdefhijklmnopqrstuvwxyz0123456789")
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func Generate() string {
	s := make([]rune, DefaultIDLength)
	for i := 0; i < len(s); i++ {
		s[i] = charset[rand.Intn(len(charset))]
	}
	return string(s)
}
